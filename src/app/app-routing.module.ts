import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppComponent as HomeComponent } from '../home/app.component';

const routes: Routes = [{
  path: '',
  component: HomeComponent
}, {
  path: 'calc',
  loadChildren: '../calculator/app.module#AppModule'
}, {
  path: 'auto-complete',
  loadChildren: '../auto-complete/app.module#AppModule'
}];

@NgModule({
  imports: [RouterModule.forRoot(routes, {useHash: true})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
