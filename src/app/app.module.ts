import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppModule as HomeModule } from '../home/app.module';

import { AppComponent } from './app.component';
import { AppNgrxModule } from './app-ngrx.module';
import { NavComponent } from './nav/nav.component';

@NgModule({
  declarations: [
    AppComponent,
    NavComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HomeModule,
    FormsModule,
    AppNgrxModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
