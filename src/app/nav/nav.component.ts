import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.scss']
})
export class NavComponent implements OnInit {

  constructor() {
    this._menuIsActive = false;
  }

  // Private variables
  private _menuIsActive: boolean;

  ngOnInit() {
  }

  // Accessors
  public get menuIsActive(): boolean {
    return this._menuIsActive;
  }

  public set menuIsActive(value: boolean) {
    this._menuIsActive = value;
  }
}
