import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Observable } from 'rxjs';
import { map, switchMap, withLatestFrom } from 'rxjs/operators';
import { AngularBasicCalculatorService } from 'angular-basic-calculator';
import { IMathExpression } from './math-expression/math-expression.interface';
import { IAction, IAnswerState, IAppStore, selectMathExpression, selectMathExpressionFromPayload } from './app-store.interface';
import { AppEnum } from './app.enum';

const appendToMathExpression = map(([mathExpressionObjFromPayload, mathExpressionObjFromStore]) => {
  let a = <IMathExpression> mathExpressionObjFromPayload;
  let b = <IMathExpression> mathExpressionObjFromStore;
  a.infixExpression = `${b.infixExpression}${a.infixExpression}`;
  return a;
});

const deleteMathExpressionTrailingCharacter = map(([action, mathExpressionObjFromStore]) => {
  let b = <IMathExpression> mathExpressionObjFromStore;
  b.infixExpression = b.infixExpression.slice(0, -1);
  return b;
});

@Injectable()
export class AppEffectsService {

  constructor(private _store: Store<IAppStore>,
              private _action$: Actions,
              private _angularBasicCalculatorService: AngularBasicCalculatorService) {
  }

  @Effect()
  appendToMathExpression$: Observable<IAction> = this._action$.pipe(
    ofType(AppEnum.MATH_OPERAND_ACTION_UPDATE, AppEnum.MATH_OPERATOR_ACTION_UPDATE),
    selectMathExpressionFromPayload,
    withLatestFrom( this._store.pipe(selectMathExpression) ),
    appendToMathExpression,
    switchMap((mathExpressionObj: IMathExpression): Iterable<IAction> => {
      return [<IAction>{
        type: AppEnum.MATH_EXPRESSION_ACTION_UPDATE,
        payload: {
          mathExpression: mathExpressionObj
        }
      }, <IAction>{
        type: AppEnum.ANSWER_ACTION_DELETE
      }];
    })
  );

  @Effect()
  deleteMathExpressionTrailingCharacter$: Observable<IAction> = this._action$.pipe(
    ofType(AppEnum.INTERRUPT_ACTION_DELETE_TRAILING),
    withLatestFrom( this._store.pipe(selectMathExpression) ),
    deleteMathExpressionTrailingCharacter,
    switchMap((mathExpressionObj: IMathExpression): Iterable<IAction> => [<IAction>{
      type: AppEnum.MATH_EXPRESSION_ACTION_UPDATE,
      payload: {
        mathExpression: mathExpressionObj
      }
    }, <IAction>{
      type: AppEnum.ANSWER_ACTION_DELETE
    }])
  );

  @Effect()
  clearMathExpression$: Observable<IAction> = this._action$.pipe(
    ofType(AppEnum.INTERRUPT_ACTION_CLEAR),
    switchMap((action: IAction): Iterable<IAction> => [<IAction>{
      type: AppEnum.MATH_EXPRESSION_ACTION_DELETE
    }, <IAction>{
      type: AppEnum.ANSWER_ACTION_DELETE
    }])
  );

  @Effect()
  INTERRUPT_ACTION_SHOW_ANSWER$: Observable<IAction> = this._action$.pipe(
    ofType(AppEnum.INTERRUPT_ACTION_SHOW_ANSWER),
    withLatestFrom( this._store.pipe(selectMathExpression) ),
    map(([action, mathExpressionObj]): IAction => {
      let infixExpression = (<IMathExpression>mathExpressionObj).infixExpression;
      return this._angularBasicCalculatorService.isExpressionValid(infixExpression) ?
        <IAction>{
          type: AppEnum.ANSWER_ACTION_UPDATE,
          payload: <IAnswerState>{
            answer: {
              value: this._angularBasicCalculatorService.parseAndEvaluate(infixExpression).toString()
            }
          }
        } : <IAction>{
          type: AppEnum.ANSWER_ACTION_NOOP
        };
    })
  );
}
