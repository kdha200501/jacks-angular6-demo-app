import { Attribute, Directive, HostListener } from '@angular/core';
import { IAction, IAppStore } from '../app-store.interface';
import { Store } from '@ngrx/store';
import { MathOperationEnum } from 'angular-basic-calculator';
import { AppEnum } from '../app.enum';

@Directive({
  selector: '[appCalculatorMathOperand]'
})
export class MathOperandDirective {

  constructor(@Attribute('value') private _value: string, private _store: Store<IAppStore>) {
  }

  @HostListener('click', ['event'])
  public onClick(): void {
    this._store.dispatch(<IAction>{
      type: AppEnum.MATH_OPERAND_ACTION_UPDATE,
      payload: {mathExpression: {infixExpression: MathOperationEnum[this._value] || this._value}}
    });
  }
}
