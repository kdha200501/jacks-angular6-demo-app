import { Attribute, Directive, HostListener } from '@angular/core';
import { IAction, IAppStore } from '../app-store.interface';
import { Store } from '@ngrx/store';
import { MathOperationEnum } from 'angular-basic-calculator';
import { AppEnum } from '../app.enum';

@Directive({
  selector: '[appCalculatorMathOperator]'
})
export class MathOperatorDirective {

  constructor(@Attribute('value') private _value: string, private _store: Store<IAppStore>) {
  }

  @HostListener('click', ['event'])
  public onClick(): void {
    this._store.dispatch(<IAction>{
      type: AppEnum.MATH_OPERATOR_ACTION_UPDATE,
      payload: {mathExpression: {infixExpression: MathOperationEnum[this._value]}}
    });
  }
}
