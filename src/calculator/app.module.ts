import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AngularBasicCalculatorModule } from 'angular-basic-calculator';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MathExpressionComponent } from './math-expression/math-expression.component';
import { AppNgrxModule } from './app-ngrx.module';
import { MathOperandDirective } from './math-operand/math-operand.directive';
import { MathOperatorDirective } from './math-operator/math-operator.directive';
import { InterruptDirective } from './interrupt/interrupt.directive';
import { AnswerComponent } from './answer/answer.component';
import { MathExpressionPipe } from './math-expression/math-expression.pipe';

@NgModule({
  imports: [
    CommonModule,
    AppRoutingModule,
    AppNgrxModule,
    AngularBasicCalculatorModule
  ],
  declarations: [AppComponent, MathExpressionComponent, MathOperandDirective, MathOperatorDirective, InterruptDirective, AnswerComponent, MathExpressionPipe]
})
export class AppModule { }
