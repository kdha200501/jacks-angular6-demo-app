import { Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { Store } from '@ngrx/store';
import { Subscription } from 'rxjs';
import { map } from 'rxjs/operators';
import { IAppStore, selectMathExpression } from '../app-store.interface';
import { MathExpression } from './math-expression.model';
import { IMathExpression } from './math-expression.interface';

@Component({
  selector: 'app-calculator-math-expression',
  templateUrl: './math-expression.component.html',
  styleUrls: ['./math-expression.component.scss']
})
export class MathExpressionComponent implements OnInit, OnDestroy {
  @ViewChild('scrollableElement', {read: ElementRef}) scrollableElement: ElementRef;

  constructor(private _store: Store<IAppStore>) {
    this._mathExpression = new MathExpression();
  }

  // Private Variables
  private _stateSubscription: Subscription;
  private _mathExpression: MathExpression;

  ngOnInit() {
    this._stateSubscription = this._store
      .pipe(
        selectMathExpression,
        map((mathExpressionObj: IMathExpression) => {

          // update display
          this._mathExpression.infixExpression = mathExpressionObj.infixExpression;

          // scroll to the right
          setTimeout(() => {
            this.scrollableElement.nativeElement.scrollTo({
              behavior: 'smooth',
              left: this.scrollableElement.nativeElement.scrollLeft + 20
            });
          }, 0);

        })
      )
      .subscribe();
  }

  ngOnDestroy() {
    this._stateSubscription.unsubscribe();
  }

  // Accessors
  get mathExpression(): MathExpression {
    return this._mathExpression;
  }
}
