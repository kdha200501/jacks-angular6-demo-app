import { IMathExpression } from './math-expression.interface';

export class MathExpression implements IMathExpression {
  constructor() {
  }

  // Private Variables
  private _infixExpression: string;

  // Accessors
  public get infixExpression(): string {
    return this._infixExpression;
  }

  public set infixExpression(value: string) {
    this._infixExpression = value;
  }
}
