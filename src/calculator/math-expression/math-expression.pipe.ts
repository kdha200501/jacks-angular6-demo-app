import { Pipe, PipeTransform } from '@angular/core';
import { AngularBasicCalculatorService, MathOperationEnum } from 'angular-basic-calculator';

const replaceOperator = (value: string): string => value
  .replace(MathOperationEnum.PLUS, '\u002B')
  .replace(MathOperationEnum.MINUS, '\u2212')
  .replace(MathOperationEnum.MULTIPLY, '\u00D7')
  .replace(MathOperationEnum.DIVIDE, '\u00F7')
  .replace(MathOperationEnum.POWER, '\uFF3E');

@Pipe({
  name: 'mathExpression'
})
export class MathExpressionPipe implements PipeTransform {

  constructor(private _angularBasicCalculatorService: AngularBasicCalculatorService) {
  }

  transform(value: string): string {
    return this._angularBasicCalculatorService.splitToken(value).map(replaceOperator).join('');
  }

}
