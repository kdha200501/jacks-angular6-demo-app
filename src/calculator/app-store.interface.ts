import { select, Action } from '@ngrx/store';
import { AppEnum } from './app.enum';
import { IMathExpression } from './math-expression/math-expression.interface';
import { IAnswer } from './answer/answer.interface';
import { pipe } from 'rxjs';
import { map } from 'rxjs/operators';

// Data structure for the store
export interface IAppStore {
  [AppEnum.STORE_NAME]: IState;
}

export interface IState extends IMathExpressionState, IAnswerState {
}

// Data structure for the states
export interface IMathExpressionState {
  mathExpression: IMathExpression;
}

export interface IAnswerState {
  answer: IAnswer;
}

// Data structure for actions
export interface IAction extends Action {
  payload?: IMathExpressionState | IAnswerState;
}

// utilities for subscribers
export const selectState = pipe( select(AppEnum.STORE_NAME), map(obj => obj[AppEnum.STORE_NAME]) );

export const selectMathExpression = pipe(selectState, map((state: IMathExpressionState) => state.mathExpression));

export const selectMathExpressionFromPayload = pipe( map((action: IAction) => (<IMathExpressionState>action.payload).mathExpression) );

export const selectAnswer = pipe(selectState, map((state: IAnswerState) => state.answer));
