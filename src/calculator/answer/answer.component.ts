import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { IAppStore, selectAnswer } from '../app-store.interface';
import { Observable } from 'rxjs';
import { IAnswer } from './answer.interface';

@Component({
  selector: 'app-calculator-answer',
  templateUrl: './answer.component.html',
  styleUrls: ['./answer.component.scss']
})
export class AnswerComponent implements OnInit {

  constructor(private _store: Store<IAppStore>) {
  }

  public answer$: Observable<IAnswer>; // the trailing '$' means observable

  ngOnInit() {
    this.answer$ = this._store.pipe(selectAnswer);
  }

}
