import { Directive, HostListener, Input } from '@angular/core';
import { IAction, IAppStore } from '../app-store.interface';
import { Store } from '@ngrx/store';
import { AppEnum } from '../app.enum';

@Directive({
  selector: '[appCalculatorInterrupt]'
})
export class InterruptDirective {

  constructor(private _store: Store<IAppStore>) {
  }

  @Input() value: string;

  @HostListener('click', ['event'])
  public onClick(): void {
    this._store.dispatch(<IAction>{
      type: AppEnum[this.value]
    });
  }
}
