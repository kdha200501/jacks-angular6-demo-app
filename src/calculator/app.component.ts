import { Component, OnDestroy, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { IAppStore, selectAnswer } from './app-store.interface';
import { map } from 'rxjs/operators';
import { Subscription } from 'rxjs';
import { IAnswer } from './answer/answer.interface';

@Component({
  selector: 'app-calculator',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy {

  constructor(private _store: Store<IAppStore>) {
  }

  // Private Variables
  private _stateSubscription: Subscription;
  private _hideAnswer: boolean;

  ngOnInit() {
    this._stateSubscription = this._store
      .pipe(
        selectAnswer,
        map((answer: IAnswer) => {
          this._hideAnswer = answer.value === '';
        })
      )
      .subscribe();
  }

  ngOnDestroy() {
    this._stateSubscription.unsubscribe();
  }

  // Accessors
  public get hideAnswer(): boolean {
    return this._hideAnswer;
  }
}
