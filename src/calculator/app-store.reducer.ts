import { InjectionToken } from '@angular/core';
import { ActionReducerMap } from '@ngrx/store';
import { AppEnum } from './app.enum';
import { IAction, IAnswerState, IAppStore, IMathExpressionState, IState } from './app-store.interface';

const initState: IState = {
  mathExpression: {
    infixExpression: ''
  },
  answer: {
    value: ''
  }
};

// use an angular token for the reducer map instead of static implementation
export const REDUCER_TOKEN = new InjectionToken<ActionReducerMap<IAppStore>>(`${AppEnum.STORE_NAME}-reducer-map`);

// provide for the angular token
export function getActionReducerMap(): ActionReducerMap<IAppStore> {
  return {
    // the token + provider approach spares the reducer map's key (which is a variable) to be statically analyzed
    [AppEnum.STORE_NAME]: function (state: IState = initState, action: IAction): IState {
      switch (action.type) {
        case AppEnum.MATH_EXPRESSION_ACTION_UPDATE:
          state.mathExpression.infixExpression = (<IMathExpressionState>action.payload).mathExpression.infixExpression;
          return {...state};
        case AppEnum.MATH_EXPRESSION_ACTION_DELETE:
          state.mathExpression.infixExpression = '';
          return {...state};
        case AppEnum.MATH_EXPRESSION_ACTION_NOOP:
          return {...state};
        case AppEnum.ANSWER_ACTION_UPDATE:
          state.answer.value = (<IAnswerState>action.payload).answer.value;
          return {...state};
        case AppEnum.ANSWER_ACTION_DELETE:
          state.answer.value = '';
          return {...state};
        default:
          return state;
      }
    }
  };
}
