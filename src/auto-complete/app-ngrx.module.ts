import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { AppEnum } from './app.enum';
import { getActionReducerMap, REDUCER_TOKEN } from './app-store.reducer';
import { AppEffectsService } from './app-effects.service';

@NgModule({
  imports: [
    CommonModule,
    StoreModule.forFeature(AppEnum.STORE_NAME, REDUCER_TOKEN),
    EffectsModule.forFeature([AppEffectsService])
  ],
  declarations: [],
  providers: [{
    provide: REDUCER_TOKEN,
    useFactory: getActionReducerMap
  }]
})
export class AppNgrxModule { }
