import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Observable } from 'rxjs';
import { debounceTime, filter, switchMap, withLatestFrom } from 'rxjs/operators';
import {
  IAction,
  IAppStore,
  ITypeAheadInputState,
  ITypeAheadResultState,
  ITypeAheadResultItemState,
  ITypeAheadResultHoverState,
  ITypeAheadStatusState,
  selectTypeAheadResultItemList,
  selectTypeAheadResultHover,
  selectTypeAheadStatus
} from './app-store.interface';
import { AppEnum } from './app.enum';
import { AppConfig } from './app.config';
import { isApiError, TypeAheadResultError, TypeAheadResultService } from './type-ahead-result/type-ahead-result.service';
import { ITypeAheadResult } from './type-ahead-result/type-ahead-result.interface';
import { ITypeAheadResultItem } from './type-ahead-result-item/type-ahead-result-item.interface';
import { ITypeAheadResultHover } from './type-ahead-result-hover/type-ahead-result-hover.interface';
import { ITypeAheadStatus } from './type-ahead-status/type-ahead-status.interface';
import { TypeAheadResult } from './type-ahead-result/type-ahead-result.model';
import { TypeAheadResultItem } from './type-ahead-result-item/type-ahead-result-item.model';
import { initState } from './app-store.reducer';

const RETRY_EXHAUSTION = 0;
const TYPE_AHEAD_INPUT_IS_STABLE_WITHIN_IN_MS = Math.max(0, AppConfig.TYPE_AHEAD_INPUT_IS_STABLE_WITHIN_IN_MS);
const TYPE_AHEAD_INPUT_MINIMUM_LENGTH = Math.max(0, AppConfig.TYPE_AHEAD_INPUT_MINIMUM_LENGTH - 1);

const distinctUntilTypeAheadInputChanged = filter(
  ([action, typeAheadStatusObj]): boolean =>
    (<ITypeAheadInputState>(<IAction>action).payload).typeAheadInput.buffer !== (<ITypeAheadStatus>typeAheadStatusObj).keyword
);

const apiIsLocked = filter(
  ([typeAheadResultObj, typeAheadStatusObj]): boolean => (<ITypeAheadStatus>typeAheadStatusObj).isLocked
);

const hasResult = filter(
  ([action, typeAheadResultItemListObj, ...otherArgs]): boolean =>
    (<Array<ITypeAheadResultItem>>typeAheadResultItemListObj).length > 0
);

@Injectable()
export class AppEffectsService {

  constructor(private _store: Store<IAppStore>,
              private _action$: Actions,
              private _typeAheadResultService: TypeAheadResultService) {
  }

  @Effect()
  INTERRUPT_TYPE_AHEAD_BUFFER$: Observable<IAction> = this._action$.pipe(
    ofType(AppEnum.INTERRUPT_TYPE_AHEAD_BUFFER),
    debounceTime(TYPE_AHEAD_INPUT_IS_STABLE_WITHIN_IN_MS),
    withLatestFrom( this._store.pipe(selectTypeAheadStatus) ),
    distinctUntilTypeAheadInputChanged,
    switchMap(([action, typeAheadStatusObj]) => {
      let actionList: Array<IAction> = [];
      let buffer = (<ITypeAheadInputState>(<IAction>action).payload).typeAheadInput.buffer;

      actionList.push(<IAction>{
        type: AppEnum.TYPE_AHEAD_INPUT_UPDATE,
        payload: <ITypeAheadInputState>{
          typeAheadInput: {buffer: buffer}
        }
      }, <IAction>{
        type: AppEnum.TYPE_AHEAD_RESULT_DELETE
      }, <IAction>{
        type: AppEnum.TYPE_AHEAD_RESULT_HOVER_DELETE
      });

      if (buffer.length > TYPE_AHEAD_INPUT_MINIMUM_LENGTH) {

        if ( (<ITypeAheadStatus>typeAheadStatusObj).isLocked ) {
          actionList.push(<IAction>{
            type: AppEnum.TYPE_AHEAD_STATUS_UPDATE,
            payload: <ITypeAheadStatusState>{
              typeAheadStatus: {...typeAheadStatusObj, keyword: buffer, expectResult: true}
            }
          });
        }
        // endIf the API is locked

        else {
          let payload = <ITypeAheadStatusState>{
            typeAheadStatus: {...typeAheadStatusObj, keyword: buffer, isLocked: true, expectResult: true} // make a snapshot of the buffer
                                                                                                          // as keyword, and lock the API
          };
          actionList.push(<IAction>{
            type: AppEnum.TYPE_AHEAD_STATUS_UPDATE,
            payload: payload
          }, <IAction>{
            type: AppEnum.INTERRUPT_TYPE_AHEAD_SEARCH,
            payload: payload
          });
        }
        // endIf the API is open

      }
      // endIf buffer is adequately long

      else {
        actionList.push(<IAction>{
          type: AppEnum.TYPE_AHEAD_STATUS_UPDATE,
          payload: <ITypeAheadStatusState>{
            typeAheadStatus: {...typeAheadStatusObj, keyword: buffer, expectResult: false}
          }
        });
      }
      // endIf buffer is too short

      return actionList;
    })
  );

  @Effect()
  INTERRUPT_TYPE_AHEAD_SEARCH$: Observable<IAction> = this._action$.pipe(
    ofType(AppEnum.INTERRUPT_TYPE_AHEAD_SEARCH),
    withLatestFrom( this._store.pipe(selectTypeAheadStatus) ),
    switchMap(([action, _typeAheadStatusObj]) => {

      let keyword = (<ITypeAheadStatusState>(<IAction>action).payload).typeAheadStatus.keyword;
      let delayInMs = (<ITypeAheadStatus>_typeAheadStatusObj).retryFib[0];

      return this._typeAheadResultService.request(keyword, delayInMs).pipe(
        withLatestFrom( this._store.pipe(selectTypeAheadStatus) ),
        apiIsLocked, // noop, if the API lock is released prior to response due to reset
        switchMap(([typeAheadResultObj, typeAheadStatusObj]) => {
          let actionList: Array<IAction> = [];
          let statusPayload = <ITypeAheadStatusState>{};

          let retryRemaining = (<ITypeAheadStatus>typeAheadStatusObj).retryRemaining;
          if ( isApiError(<TypeAheadResultError>typeAheadResultObj) && retryRemaining !== RETRY_EXHAUSTION ) {
            let retryFib = [...(<ITypeAheadStatus>typeAheadStatusObj).retryFib];
            retryFib.push( retryFib.shift() + retryFib[0] );
            statusPayload = <ITypeAheadStatusState>{
              typeAheadStatus: {
                ...typeAheadStatusObj,
                retryRemaining: Math.max(RETRY_EXHAUSTION, retryRemaining - 1), // update the count of remaining retry
                retryFib: retryFib // prepare the next two retry delays
              }
            };
            actionList.push(<IAction>{
              type: AppEnum.TYPE_AHEAD_STATUS_UPDATE,
              payload: statusPayload
            }, <IAction>{
              type: AppEnum.INTERRUPT_TYPE_AHEAD_SEARCH, // repeat the request with the latest keyword
              payload: statusPayload
            });
            return actionList;
          }
          // endIf the API timed-out and retry is allowed

          // Note: the following will be invoked if the API does not error out, or
          // the API errors out and it has exhausted retries

          if (keyword !== (<ITypeAheadStatus>typeAheadStatusObj).keyword) {
            statusPayload = <ITypeAheadStatusState>{
              typeAheadStatus: {...typeAheadStatusObj, retryRemaining: initState.typeAheadStatus.retryRemaining}  // reset the count of
                                                                                                                  // remaining retry
            };
            actionList.push(<IAction>{
              type: AppEnum.TYPE_AHEAD_STATUS_UPDATE,
              payload: statusPayload
            }, <IAction>{
              type: AppEnum.TYPE_AHEAD_RESULT_DELETE
            }, <IAction>{
              type: AppEnum.TYPE_AHEAD_RESULT_HOVER_DELETE
            }, <IAction>{
              type: AppEnum.INTERRUPT_TYPE_AHEAD_SEARCH, // repeat the request with the latest keyword
              payload: statusPayload
            });
            return actionList;
          }
          // endIf the keyword has changed since this request is made

          let typeAheadResult = new TypeAheadResult(<ITypeAheadResult>typeAheadResultObj);
          statusPayload = <ITypeAheadStatusState>{
            typeAheadStatus: {
              keyword: keyword,
              isLocked: false, // release the lock
              expectResult: typeAheadResult.isSuccessful && typeAheadResult.isFound,
              retryRemaining: initState.typeAheadStatus.retryRemaining, // reset the count of remaining retry
              retryFib: initState.typeAheadStatus.retryFib // reset the retry delays
            }
          };
          actionList.push(<IAction>{
            type: AppEnum.TYPE_AHEAD_STATUS_UPDATE,
            payload: statusPayload
          }, <IAction>{
            type: AppEnum.TYPE_AHEAD_RESULT_UPDATE,
            payload: <ITypeAheadResultState>{
              typeAheadResultItemList: typeAheadResult.typeAheadResultItemList // update the results
            }
          }, <IAction>{
            type: AppEnum.TYPE_AHEAD_RESULT_HOVER_DELETE
          });

          return actionList;
        })
      );

    })
  );

  @Effect()
  INTERRUPT_TYPE_AHEAD_SELECT$: Observable<IAction> = this._action$.pipe(
    ofType(AppEnum.INTERRUPT_TYPE_AHEAD_SELECT),
    withLatestFrom( this._store.pipe(selectTypeAheadStatus) ),
    switchMap(([action, typeAheadStatusObj]) => {

      let typeAheadResultItem = new TypeAheadResultItem( (<ITypeAheadResultItemState>(<IAction>action).payload).typeAheadResultItem );

      return [<IAction>{
        type: AppEnum.TYPE_AHEAD_INPUT_UPDATE,
        payload: <ITypeAheadInputState>{
          typeAheadInput: {buffer: typeAheadResultItem.title}
        }
      }, <IAction>{
        type: AppEnum.TYPE_AHEAD_STATUS_UPDATE,
        payload: <ITypeAheadStatusState>{
          typeAheadStatus: {...typeAheadStatusObj, expectResult: false}
        }
      }, <IAction>{
        type: AppEnum.TYPE_AHEAD_RESULT_DELETE
      }, <IAction>{
        type: AppEnum.TYPE_AHEAD_RESULT_HOVER_DELETE
      }];

    })
  );

  @Effect()
  INTERRUPT_TYPE_AHEAD_HOVER$: Observable<IAction> = this._action$.pipe(
    ofType(AppEnum.INTERRUPT_TYPE_AHEAD_HOVER),
    withLatestFrom(
      this._store.pipe(selectTypeAheadResultItemList),
      this._store.pipe(selectTypeAheadResultHover),
      this._store.pipe(selectTypeAheadStatus)
    ),
    hasResult,
    switchMap(([action, typeAheadResultItemListObj, typeAheadResultHoverObj, typeAheadStatusObj]) => {

      let typeAheadResultItemObjList = <Array<ITypeAheadResultItem>>typeAheadResultItemListObj;

      let idx = (<ITypeAheadResultHover>typeAheadResultHoverObj).idx +
        (<ITypeAheadResultHoverState>(<IAction>action).payload).typeAheadResultHover.idx;

      let listSize = Math.min(AppConfig.TYPE_AHEAD_RESULT_MAXIMUM_LENGTH, typeAheadResultItemObjList.length);

      idx = Math.max(0, idx) % (listSize + 1);

      let buffer = idx === 0 ?
        (<ITypeAheadStatus>typeAheadStatusObj).keyword :
        new TypeAheadResultItem(typeAheadResultItemObjList[idx - 1]).title;

      return [<IAction>{
        type: AppEnum.TYPE_AHEAD_INPUT_UPDATE,
        payload: <ITypeAheadInputState>{
          typeAheadInput: {buffer: buffer}
        }
      }, <IAction>{
        type: AppEnum.TYPE_AHEAD_RESULT_HOVER_UPDATE,
        payload: <ITypeAheadResultHoverState>{
          typeAheadResultHover: {idx: idx}
        }
      }];

    })
  );

  @Effect()
  INTERRUPT_REDIRECT_TO_SEARCH_RESULT$: Observable<IAction> = this._action$.pipe(
    ofType(AppEnum.INTERRUPT_REDIRECT_TO_SEARCH_RESULT),
    withLatestFrom( this._store.pipe(selectTypeAheadStatus) ),
    switchMap(([action, typeAheadStatusObj]) => [<IAction>{
      type: AppEnum.TYPE_AHEAD_RESULT_DELETE
    }, <IAction>{
      type: AppEnum.TYPE_AHEAD_RESULT_HOVER_DELETE
    }, <IAction>{
      type: AppEnum.TYPE_AHEAD_STATUS_UPDATE,
      payload: <ITypeAheadStatusState>{
        typeAheadStatus: {...typeAheadStatusObj, expectResult: false}
      }
    }])
  );

}
