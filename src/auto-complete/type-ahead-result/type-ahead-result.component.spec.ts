import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TypeAheadResultComponent } from './type-ahead-result.component';

describe('TypeAheadResultComponent', () => {
  let component: TypeAheadResultComponent;
  let fixture: ComponentFixture<TypeAheadResultComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TypeAheadResultComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TypeAheadResultComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
