import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { distinctUntilKeyChanged, map } from 'rxjs/operators';
import {
  IAction,
  IAppStore,
  ITypeAheadResultItemState,
  selectTypeAheadResultItemList,
  selectTypeAheadResultHover
} from '../app-store.interface';
import { AppEnum } from '../app.enum';
import { AppConfig } from '../app.config';
import { TypeAheadResultItem } from '../type-ahead-result-item/type-ahead-result-item.model';
import { ITypeAheadResultItem } from '../type-ahead-result-item/type-ahead-result-item.interface';
import { ITypeAheadResultHover } from '../type-ahead-result-hover/type-ahead-result-hover.interface';

const TYPE_AHEAD_RESULT_MAXIMUM_LENGTH = Math.max(0, AppConfig.TYPE_AHEAD_RESULT_MAXIMUM_LENGTH);

const limitResultSize = map(
  (typeAheadResultItemObjList: Array<ITypeAheadResultItem>): Array<ITypeAheadResultItem> =>
    typeAheadResultItemObjList.slice(0, TYPE_AHEAD_RESULT_MAXIMUM_LENGTH)
);

const instantiateTypeAheadResultItem = (typeAheadResultItemObj: ITypeAheadResultItem): TypeAheadResultItem =>
  new TypeAheadResultItem(typeAheadResultItemObj);

const instantiateTypeAheadResultItemList = map(
  (typeAheadResultItemObjList: Array<ITypeAheadResultItem>): Array<TypeAheadResultItem> =>
    typeAheadResultItemObjList.map(instantiateTypeAheadResultItem)
);

@Component({
  selector: 'app-auto-complete-type-ahead-result',
  templateUrl: './type-ahead-result.component.html',
  styleUrls: ['./type-ahead-result.component.scss']
})
export class TypeAheadResultComponent implements OnInit {

  constructor(private _store: Store<IAppStore>) {
  }

  // Public Variables
  public typeAheadResultItemList$: Observable<Array<TypeAheadResultItem>>; // the trailing '$' means observable
  public typeAheadResultHover$: Observable<ITypeAheadResultHover>;

  ngOnInit() {
    this.typeAheadResultItemList$ = this._store.pipe(
      selectTypeAheadResultItemList,
      limitResultSize,
      instantiateTypeAheadResultItemList
    );
    this.typeAheadResultHover$ = this._store.pipe(
      selectTypeAheadResultHover,
      distinctUntilKeyChanged('idx')
    );
  }

  public onSelectTypeAheadResultItem(typeAheadResultItemObj: ITypeAheadResultItem): void {
    this._store.dispatch(<IAction>{
      type: AppEnum.INTERRUPT_TYPE_AHEAD_SELECT,
      payload: <ITypeAheadResultItemState>{
        typeAheadResultItem: typeAheadResultItemObj
      }
    });
  }

}
