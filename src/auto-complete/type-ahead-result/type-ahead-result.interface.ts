import { ITypeAheadResultItem } from '../type-ahead-result-item/type-ahead-result-item.interface';

export interface ITypeAheadResult {
  'Search': Array<ITypeAheadResultItem>;
  'totalResults': string;
  'Response': string;
}
