import { ITypeAheadResult } from './type-ahead-result.interface';
import { ITypeAheadResultItem } from '../type-ahead-result-item/type-ahead-result-item.interface';

export class TypeAheadResult {
  constructor(private _typeAheadResultObj: ITypeAheadResult) {
  }

  // Accessors
  public get typeAheadResultItemList(): Array<ITypeAheadResultItem> {
    return this._typeAheadResultObj.Search || [];
  }

  public get isSuccessful(): boolean {
    return (typeof this._typeAheadResultObj.Response).toLowerCase() === 'string' &&
      this._typeAheadResultObj.Response.trim().toLowerCase() === 'true';

  }

  public get isFound(): boolean {
    return this.typeAheadResultItemList.length > 0;
  }
}
