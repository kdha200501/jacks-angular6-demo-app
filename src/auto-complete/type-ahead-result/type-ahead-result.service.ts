import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpParams } from '@angular/common/http';
import { Observable, of, TimeoutError, timer } from 'rxjs';
import { catchError, switchMap, timeout } from 'rxjs/operators';
import { AppConfig } from '../app.config';
import { ITypeAheadResult } from './type-ahead-result.interface';

export type TypeAheadResultError = TimeoutError | HttpErrorResponse;

export const isApiError = (typeAheadResultError: TypeAheadResultError): boolean =>
  typeAheadResultError.name === TimeoutError.name || typeAheadResultError.name === HttpErrorResponse.name;

const API_TIMEOUT_IN_MS = Math.max(0, AppConfig.API_TIMEOUT_IN_MS);

/*
const mockResponse = of({
  'Search': [
    {
      'Title': 'Iron Man',
      'Year': '2008',
      'imdbID': 'tt0371746',
      'Type': 'movie',
      'Poster': 'https://m.media-amazon.com/images/M/MV5BMTczNTI2ODUwOF5BMl5BanBnXkFtZTcwMTU0NTIzMw@@._V1_SX300.jpg'
    },
    {
      'Title': 'Iron Man 3',
      'Year': '2013',
      'imdbID': 'tt1300854',
      'Type': 'movie',
      'Poster': 'https://m.media-amazon.com/images/M/MV5BMjE5MzcyNjk1M15BMl5BanBnXkFtZTcwMjQ4MjcxOQ@@._V1_SX300.jpg'
    },
    {
      'Title': 'Iron Man 2',
      'Year': '2010',
      'imdbID': 'tt1228705',
      'Type': 'movie',
      'Poster': 'https://m.media-amazon.com/images/M/MV5BMTM0MDgwNjMyMl5BMl5BanBnXkFtZTcwNTg3NzAzMw@@._V1_SX300.jpg'
    }
  ],
  'totalResults': '82',
  'Response': 'True'
}).pipe(
  delay(500)
);
*/

@Injectable()
export class TypeAheadResultService {

  constructor(private _httpClient: HttpClient) { }

  public request(keyword: string, delayInMs: number = 0): Observable<ITypeAheadResult | TypeAheadResultError> {
    let httpParams = new HttpParams()
      .append(AppConfig.API_TOKEN_KEY, AppConfig.API_TOKEN_VALUE)
      .append(AppConfig.API_SEARCH_KEY, keyword);

    console.log(
      `[api] httpParams: ${httpParams.toString()}, delayInMs: ${delayInMs}`
    );

    return timer(delayInMs)
      .pipe(
        switchMap(() => this._httpClient.get<ITypeAheadResult>(AppConfig.API_URL, {params: httpParams})
          .pipe(
            timeout(API_TIMEOUT_IN_MS),
            catchError((error: TypeAheadResultError) => of(error))
          )
        )
      );
  }
}
