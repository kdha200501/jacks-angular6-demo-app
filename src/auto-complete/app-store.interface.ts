import { select, Action } from '@ngrx/store';
import { pipe } from 'rxjs';
import { map } from 'rxjs/operators';
import { AppEnum } from './app.enum';
import { ITypeAheadInput } from './type-ahead-input/type-ahead-input.interface';
import { ITypeAheadResultItem } from './type-ahead-result-item/type-ahead-result-item.interface';
import { ITypeAheadResultHover } from './type-ahead-result-hover/type-ahead-result-hover.interface';
import { ITypeAheadStatus } from './type-ahead-status/type-ahead-status.interface';

// Data structure for the store
export interface IAppStore {
  [AppEnum.STORE_NAME]: IState;
}

export interface IState extends ITypeAheadInputState, ITypeAheadResultState, ITypeAheadResultItemState, ITypeAheadResultHoverState,
  ITypeAheadStatusState {
}

// Data structure for states
export interface ITypeAheadInputState {
  typeAheadInput: ITypeAheadInput;
}
export interface ITypeAheadResultState {
  typeAheadResultItemList: Array<ITypeAheadResultItem>;
}
export interface ITypeAheadResultItemState {
  typeAheadResultItem?: ITypeAheadResultItem;
}
export interface ITypeAheadResultHoverState {
  typeAheadResultHover?: ITypeAheadResultHover;
}
export interface ITypeAheadStatusState {
  typeAheadStatus: ITypeAheadStatus;
}

// Data structure for actions
export interface IAction extends Action {
  payload?: ITypeAheadInputState | ITypeAheadResultState | ITypeAheadResultItemState | ITypeAheadResultHoverState | ITypeAheadStatusState;
}

// utilities for subscribers
const selectState = pipe( select(AppEnum.STORE_NAME), map(obj => obj[AppEnum.STORE_NAME]) );

export const selectTypeAheadInput = pipe(selectState, map((state: ITypeAheadInputState) => state.typeAheadInput));
export const selectTypeAheadResultItemList = pipe(selectState, map((state: ITypeAheadResultState) => state.typeAheadResultItemList));
export const selectTypeAheadResultHover = pipe(selectState, map((state: ITypeAheadResultHoverState) => state.typeAheadResultHover));
export const selectTypeAheadStatus = pipe(selectState, map((state: ITypeAheadStatusState) => state.typeAheadStatus));
