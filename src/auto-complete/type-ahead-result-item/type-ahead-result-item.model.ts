import { ITypeAheadResultItem } from './type-ahead-result-item.interface';
import { AppEnum } from '../app.enum';

export class TypeAheadResultItem {
  constructor(private _typeAheadResultItemObj: ITypeAheadResultItem) {
  }

  // Public Methods
  public toJson(): ITypeAheadResultItem {
    return this._typeAheadResultItemObj;
  }

  // Accessors
  get title(): string {
    return this._typeAheadResultItemObj.Title;
  }
  get year(): string {
    return this._typeAheadResultItemObj.Year;
  }
  get imdbId(): string {
    return this._typeAheadResultItemObj.imdbID;
  }
  get isTv(): boolean {
    return this._typeAheadResultItemObj.Type === AppEnum.TYPE_AHEAD_RESULT_ITEM_TYPE_TV;
  }
  get isFilm(): boolean {
    return this._typeAheadResultItemObj.Type === AppEnum.TYPE_AHEAD_RESULT_ITEM_TYPE_FILM;
  }
  get posterUrl(): string {
    return this._typeAheadResultItemObj.Poster;
  }
}
