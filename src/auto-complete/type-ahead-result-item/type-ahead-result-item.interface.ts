export interface ITypeAheadResultItem {
  'Title': string;
  'Year': string;
  'imdbID': string;
  'Type': string;
  'Poster': string;
}
