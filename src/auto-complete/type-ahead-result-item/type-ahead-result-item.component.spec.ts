import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TypeAheadResultItemComponent } from './type-ahead-result-item.component';

describe('TypeAheadResultItemComponent', () => {
  let component: TypeAheadResultItemComponent;
  let fixture: ComponentFixture<TypeAheadResultItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TypeAheadResultItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TypeAheadResultItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
