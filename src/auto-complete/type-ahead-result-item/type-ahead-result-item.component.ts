import { Component, EventEmitter, Input, Output } from '@angular/core';
import { TypeAheadResultItem } from './type-ahead-result-item.model';
import { ITypeAheadResultItem } from './type-ahead-result-item.interface';

@Component({
  selector: 'app-auto-complete-type-ahead-result-item',
  templateUrl: './type-ahead-result-item.component.html',
  styleUrls: ['./type-ahead-result-item.component.scss']
})
export class TypeAheadResultItemComponent {
  @Input() typeAheadResultItem: TypeAheadResultItem;
  @Output() selectTypeAheadResultItem: EventEmitter<ITypeAheadResultItem> = new EventEmitter();

  constructor() {
  }

  public onClick(): void {
    this.selectTypeAheadResultItem.emit( this.typeAheadResultItem.toJson() );
  }

}
