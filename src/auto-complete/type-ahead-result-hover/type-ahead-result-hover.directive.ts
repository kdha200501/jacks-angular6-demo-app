import { Directive, HostListener, Input } from '@angular/core';
import { Store } from '@ngrx/store';
import { IAction, IAppStore, ITypeAheadResultHoverState } from '../app-store.interface';
import { TypeAheadInput } from '../type-ahead-input/type-ahead-input.model';
import { AppEnum } from '../app.enum';

const isArrowKey = (keyboardEvent: KeyboardEvent): boolean =>
  keyboardEvent.code === AppEnum.EVENT_CODE_ARROW_DOWN || keyboardEvent.code === AppEnum.EVENT_CODE_ARROW_UP;

const convertArrowToIdx = (keyboardEvent: KeyboardEvent): number => {
  let idx = 0;
  if (keyboardEvent.code === AppEnum.EVENT_CODE_ARROW_DOWN) {
    idx++;
  }
  if (keyboardEvent.code === AppEnum.EVENT_CODE_ARROW_UP) {
    idx--;
  }
  return idx;
};

@Directive({
  selector: '[appAutoCompleteTypeAheadResultHover]'
})
export class TypeAheadResultHoverDirective {
  @Input()typeAheadInput: TypeAheadInput;

  constructor(private _store: Store<IAppStore>) {
  }

  // Private Methods
  private handleArrowEvent(keyboardEvent: KeyboardEvent): void {
    if ( !isArrowKey(keyboardEvent) ) {
      return;
    }
    // endIf not arrow key event

    if (keyboardEvent.type === AppEnum.EVENT_TYPE_KEY_UP) {
      this.typeAheadInput.isReactive = false;
      return;
    }
    // endIf key-up event

    if (this.typeAheadInput.isReactive) {
      return;
    }
    // endIf the input is acting as a receiver

    if (keyboardEvent.type === AppEnum.EVENT_TYPE_KEY_DOWN) {
      this.typeAheadInput.isReactive = true;
      this._store.dispatch(<IAction>{
        type: AppEnum.INTERRUPT_TYPE_AHEAD_HOVER,
        payload: <ITypeAheadResultHoverState>{
          typeAheadResultHover: {idx: convertArrowToIdx(keyboardEvent)}
        }
      });
    }
    // endIf key-down event

    console.log(
      `key type: ${keyboardEvent.type}, key code:${keyboardEvent.code}`
    );
  }

  @HostListener(AppEnum.EVENT_TYPE_KEY_DOWN, ['$event'])
  public keyDown(keyboardEvent: KeyboardEvent): void {
    this.handleArrowEvent(keyboardEvent);
  }

  @HostListener(AppEnum.EVENT_TYPE_KEY_UP, ['$event'])
  public keyUp(keyboardEvent: KeyboardEvent): void {
    this.handleArrowEvent(keyboardEvent);
  }

}
