import { Component, ElementRef, HostBinding, Input, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable, Subject, Subscription } from 'rxjs';
import { distinctUntilKeyChanged, filter, map } from 'rxjs/operators';
import { TypeAheadInput } from './type-ahead-input.model';
import {
  IAction,
  IAppStore,
  selectTypeAheadInput,
  selectTypeAheadResultHover,
  ITypeAheadInputState
} from '../app-store.interface';
import { ITypeAheadInput } from './type-ahead-input.interface';
import { ITypeAheadResultHover } from '../type-ahead-result-hover/type-ahead-result-hover.interface';
import { AppEnum } from '../app.enum';

const isEscape = filter(
  (keyboardEvent: KeyboardEvent): boolean => keyboardEvent.code === AppEnum.EVENT_CODE_ESCAPE
);

@Component({
  selector: 'app-auto-complete-type-ahead-input',
  templateUrl: './type-ahead-input.component.html',
  styleUrls: ['./type-ahead-input.component.scss']
})
export class TypeAheadInputComponent implements OnInit, OnDestroy {
  @ViewChild('inputElement') inputElement: ElementRef;

  @HostBinding('attr.is-empty')
  @Input() isEmpty: boolean;

  constructor(private _store: Store<IAppStore>) {
    this._typeAheadInput = new TypeAheadInput();
    this._typeAheadInput.isReactive = true; // the input is blur by default
    this.isEmpty = true; // the input is empty by default
    this.keyboardEvent$ = new Subject<KeyboardEvent>();
  }

  // Private Variables
  private _typeAheadInput: TypeAheadInput;
  private _stateSubscription: Subscription;

  // Public Variables
  public typeAheadResultHover$: Observable<ITypeAheadResultHover>; // the trailing '$' means observable
  public keyboardEvent$: Subject<KeyboardEvent>;

  ngOnInit() {
    this._stateSubscription = this._store
      .pipe(
        selectTypeAheadInput,
        map((typeAheadInputObj: ITypeAheadInput) => {
          if (this._typeAheadInput.isReactive) {
            this.buffer = typeAheadInputObj.buffer; // update input
            this.inputElement.nativeElement.focus();
          }
          // endIf the input is acting as a receiver
        })
      )
      .subscribe();

    this.typeAheadResultHover$ = this._store.pipe(
      selectTypeAheadResultHover,
      distinctUntilKeyChanged('idx')
    );

    this.keyboardEvent$
      .pipe(
        isEscape,
        map((keyboardEvent: KeyboardEvent) => {
          if (keyboardEvent.type === AppEnum.EVENT_TYPE_KEY_UP) {
            this.typeAheadInput.isReactive = false;
            return;
          }
          // endIf key-up event

          if (this.typeAheadInput.isReactive) {
            return;
          }
          // endIf the input is acting as a receiver

          if (keyboardEvent.type === AppEnum.EVENT_TYPE_KEY_DOWN) {
            this.typeAheadInput.isReactive = true;
            this._store.dispatch(<IAction>{
              type: AppEnum.TYPE_AHEAD_RESET
            });
          }
          // endIf key-down event
        })
      )
      .subscribe();
  }

  ngOnDestroy() {
    this._stateSubscription.unsubscribe();
    this.keyboardEvent$.unsubscribe();
  }

  // Private Methods
  private emmitAction(): void {
    this._store.dispatch(<IAction>{
      type: AppEnum.INTERRUPT_TYPE_AHEAD_BUFFER,
      payload: <ITypeAheadInputState>{
        typeAheadInput: this._typeAheadInput.toJson()
      }
    });
  }

  // Public Methods
  public onBlur(): void {
    this._typeAheadInput.isReactive = true;
  }

  public onFocus(): void {
    this._typeAheadInput.isReactive = false;
  }

  // Accessors
  public get typeAheadInput(): TypeAheadInput {
    return this._typeAheadInput;
  }

  public get buffer(): string {
    return this._typeAheadInput.buffer;
  }

  public set buffer(value: string) {
    this._typeAheadInput.buffer = value;
    this.isEmpty = value === '';

    console.log(
      '[input component accessor] value:', value
    );

    if (!this._typeAheadInput.isReactive) {
      this.emmitAction();
    }
    // endIf the input is acting as a source
  }
}
