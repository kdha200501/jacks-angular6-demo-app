export interface ITypeAheadInput {
  buffer: string;
  isReactive?: boolean;
}
