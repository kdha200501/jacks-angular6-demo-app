import { ITypeAheadInput } from './type-ahead-input.interface';

export class TypeAheadInput implements ITypeAheadInput {
  constructor() {
  }

  // Private Variables
  private _buffer: string;
  private _isReactive: boolean;

  // Public Methods
  public toJson(): ITypeAheadInput {
    return {
      buffer: this._buffer
    };
  }

  // Accessors
  public get buffer(): string {
    return this._buffer;
  }

  public set buffer(value: string) {
    this._buffer = value;
  }

  public get isReactive(): boolean {
    return this._isReactive;
  }

  public set isReactive(value: boolean) {
    this._isReactive = value;
  }
}
