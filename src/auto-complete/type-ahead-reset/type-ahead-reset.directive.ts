import { Directive, HostListener } from '@angular/core';
import { Store } from '@ngrx/store';
import { IAppStore } from '../app-store.interface';
import { IAction } from '../app-store.interface';
import { AppEnum } from '../app.enum';

@Directive({
  selector: '[appAutoCompleteTypeAheadReset]'
})
export class TypeAheadResetDirective {

  constructor(private _store: Store<IAppStore>) {
  }

  @HostListener(AppEnum.EVENT_TYPE_CLICK, ['$event'])
  public onClick(): void {
    this._store.dispatch(<IAction>{
      type: AppEnum.TYPE_AHEAD_RESET
    });
  }

}
