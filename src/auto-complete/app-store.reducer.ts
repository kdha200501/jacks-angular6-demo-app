import { InjectionToken } from '@angular/core';
import { ActionReducerMap } from '@ngrx/store';
import { AppEnum } from './app.enum';
import { AppConfig } from './app.config';
import {
  IAction,
  IAppStore,
  IState,
  ITypeAheadInputState,
  ITypeAheadResultState,
  ITypeAheadResultHoverState,
  ITypeAheadStatusState
} from './app-store.interface';

const API_RETRY_MAXIMUM_COUNT = Math.max(0, AppConfig.API_RETRY_MAXIMUM_COUNT);
const API_RETRY_BACK_OFF_INIT_DELAY_IN_MS = Math.max(0, AppConfig.API_RETRY_BACK_OFF_INIT_DELAY_IN_MS);

export const initState: IState = {
  typeAheadInput: {
    buffer: ''
  },
  typeAheadResultItemList: [],
  typeAheadResultHover: {
    idx: 0
  },
  typeAheadStatus: {
    keyword: '',
    isLocked: false,
    expectResult: false,
    retryRemaining: API_RETRY_MAXIMUM_COUNT,
    retryFib: [0, API_RETRY_BACK_OFF_INIT_DELAY_IN_MS]
  }
};

// use an angular token for the reducer map instead of static implementation
export const REDUCER_TOKEN = new InjectionToken<ActionReducerMap<IAppStore>>(`${AppEnum.STORE_NAME}-reducer-map`);

// provide for the angular token
export function getActionReducerMap(): ActionReducerMap<IAppStore> {
  return {
    // the token + provider approach spares the reducer map's key (which is a variable) to be statically analyzed
    [AppEnum.STORE_NAME]: function (state: IState = initState, action: IAction): IState {
      switch (action.type) {

        case AppEnum.TYPE_AHEAD_INPUT_UPDATE:
          console.log(
            '[reducer] update input'
          );
          return {...state, typeAheadInput: (<ITypeAheadInputState>action.payload).typeAheadInput};

        case AppEnum.TYPE_AHEAD_STATUS_UPDATE:
          console.log(
            '[reducer] update status'
          );
          return {...state, typeAheadStatus: (<ITypeAheadStatusState>action.payload).typeAheadStatus};

        case AppEnum.TYPE_AHEAD_RESULT_UPDATE:
          console.log(
            '[reducer] update result'
          );
          return {...state, typeAheadResultItemList: (<ITypeAheadResultState>action.payload).typeAheadResultItemList};

        case AppEnum.TYPE_AHEAD_RESULT_DELETE:
          console.log(
            '[reducer] delete result'
          );
          return {...state, typeAheadResultItemList: [...initState.typeAheadResultItemList]};

        case AppEnum.TYPE_AHEAD_RESULT_HOVER_UPDATE:
          console.log(
            '[reducer] update result hover'
          );
          return {...state, typeAheadResultHover: (<ITypeAheadResultHoverState>action.payload).typeAheadResultHover};

        case AppEnum.TYPE_AHEAD_RESULT_HOVER_DELETE:
          console.log(
            '[reducer] delete result hover'
          );
          return {...state, typeAheadResultHover: initState.typeAheadResultHover};

        case AppEnum.TYPE_AHEAD_RESET:
          console.log(
            '[reducer] reset'
          );
          return {...initState}; // mutates state, broadcasts state

        default: // initial and effects
          console.log(
            '[reducer] initial and effects'
          );
          return state; // does not mutate state, does not broadcast
      }
    }
  };
}
