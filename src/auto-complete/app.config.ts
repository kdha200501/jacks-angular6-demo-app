export const AppConfig = {
  TYPE_AHEAD_INPUT_IS_STABLE_WITHIN_IN_MS: 500, // the window of time within which, no type-ahead input event is detected
  TYPE_AHEAD_INPUT_MINIMUM_LENGTH: 2, // the minimum number of characters of a type-ahead input that would trigger an auto-complete search
  TYPE_AHEAD_RESULT_MAXIMUM_LENGTH: 5, // the maximum number of auto-complete results to display
  API_TIMEOUT_IN_MS: 500, // the maximum wait time for an API response
  API_RETRY_MAXIMUM_COUNT: 3, // the maximum number of retry attempts after API errors out
  API_RETRY_BACK_OFF_INIT_DELAY_IN_MS: 1000, // the initial delay for the retry's back off sequence

  // API
  API_URL: 'https://www.omdbapi.com',
  API_TOKEN_KEY: 'apikey',
  API_TOKEN_VALUE: 'dbac759b',
  API_SEARCH_KEY: 's',

  // Redirect
  SEARCH_REDIRECT_URL: 'https://www.imdb.com/find',
  SEARCH_REDIRECT_SEARCH_KEY: 'q'
};
