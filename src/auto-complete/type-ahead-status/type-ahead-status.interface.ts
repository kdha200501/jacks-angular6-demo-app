export interface ITypeAheadStatus {
  keyword: string;
  isLocked: boolean;
  expectResult: boolean;
  retryRemaining: number;
  retryFib: Array<number>;
}
