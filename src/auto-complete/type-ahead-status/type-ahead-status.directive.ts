import { Directive, HostBinding, Input, OnDestroy, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { map } from 'rxjs/operators';
import { Subscription } from 'rxjs';
import { IAppStore, selectTypeAheadStatus } from '../app-store.interface';
import { ITypeAheadStatus } from './type-ahead-status.interface';

@Directive({
  selector: '[appAutoCompleteTypeAheadStatus]'
})
export class TypeAheadStatusDirective implements OnInit, OnDestroy {
  @HostBinding('attr.is-pending')
  @Input() isPending: boolean;

  @HostBinding('attr.expect-result')
  @Input() expectResult: boolean;

  constructor(private _store: Store<IAppStore>) {
  }

  // Private Variables
  private _stateSubscription: Subscription;

  ngOnInit() {
    this._stateSubscription = this._store
      .pipe(
        selectTypeAheadStatus,
        map((typeAheadStatusObj: ITypeAheadStatus) => {
          this.isPending = typeAheadStatusObj.isLocked;
          this.expectResult = typeAheadStatusObj.expectResult;
        })
      )
      .subscribe();
  }

  ngOnDestroy() {
    this._stateSubscription.unsubscribe();
  }

}
