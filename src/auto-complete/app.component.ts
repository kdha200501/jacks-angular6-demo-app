import { Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { Store } from '@ngrx/store';
import { Subscription } from 'rxjs';
import { distinctUntilKeyChanged, map } from 'rxjs/operators';
import { IAction, IAppStore, selectTypeAheadInput } from './app-store.interface';
import { ITypeAheadInput } from './type-ahead-input/type-ahead-input.interface';
import { AppConfig } from './app.config';
import { AppEnum } from './app.enum';
import { TypeAheadInput } from './type-ahead-input/type-ahead-input.model';

@Component({
  selector: 'app-auto-complete',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy {
  @ViewChild('formElement') formElement: ElementRef;

  constructor(private _store: Store<IAppStore>) {
    this._typeAheadInput = new TypeAheadInput();
    this._typeAheadInput.buffer = null;
  }

  // Private Variables
  private _typeAheadInput: TypeAheadInput;
  private _stateSubscription: Subscription;

  ngOnInit() {
    this._stateSubscription = this._store
      .pipe(
        selectTypeAheadInput,
        distinctUntilKeyChanged('buffer'),
        map((typeAheadInputObj: ITypeAheadInput) => {
          this.buffer = typeAheadInputObj.buffer;
        })
      )
      .subscribe();
  }

  ngOnDestroy() {
    this._stateSubscription.unsubscribe();
  }

  // Private Methods
  private redirectToSearchResult(): void {
    this._store.dispatch(<IAction>{
      type: AppEnum.INTERRUPT_REDIRECT_TO_SEARCH_RESULT
    });
    this.formElement.nativeElement.submit();
  }

  // Public Methods
  public resetInput() {
    this._store.dispatch(<IAction>{
      type: AppEnum.TYPE_AHEAD_RESET
    });
  }

  public submit(): void {
    if (this.buffer === null) {
      return;
    }
    // endIf search URL is not formed yet

    this.redirectToSearchResult();
  }

  // Accessors
  public get buffer(): string {
    return this._typeAheadInput.buffer;
  }

  public set buffer(value: string) {
    this._typeAheadInput.buffer = value;
  }

  public get searchRedirectUrl(): string {
    return AppConfig.SEARCH_REDIRECT_URL;
  }

  public get searchRedirectSearchKey(): string {
    return AppConfig.SEARCH_REDIRECT_SEARCH_KEY;
  }
}
