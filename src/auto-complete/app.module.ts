import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppNgrxModule } from './app-ngrx.module';

import { TypeAheadResultService } from './type-ahead-result/type-ahead-result.service';

import { AppComponent } from './app.component';
import { TypeAheadInputComponent } from './type-ahead-input/type-ahead-input.component';
import { TypeAheadResultComponent } from './type-ahead-result/type-ahead-result.component';
import { TypeAheadResultItemComponent } from './type-ahead-result-item/type-ahead-result-item.component';
import { TypeAheadResultHoverDirective } from './type-ahead-result-hover/type-ahead-result-hover.directive';
import { TypeAheadStatusDirective } from './type-ahead-status/type-ahead-status.directive';
import { TypeAheadResetDirective } from './type-ahead-reset/type-ahead-reset.directive';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    HttpClientModule,
    AppRoutingModule,
    AppNgrxModule
  ],
  providers: [TypeAheadResultService],
  declarations: [AppComponent, TypeAheadInputComponent, TypeAheadResultComponent, TypeAheadResultItemComponent,
    TypeAheadResultHoverDirective, TypeAheadStatusDirective, TypeAheadResetDirective]
})
export class AppModule { }
